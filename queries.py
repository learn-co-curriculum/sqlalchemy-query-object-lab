from sqlalchemy import create_engine
from seed import Company
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///dow_jones.db', echo=True)

Session = sessionmaker(bind=engine)
session = Session()

def return_apple():
    pass

def return_disneys_industry():
    pass

def return_list_of_company_objects_ordered_alphabetically_by_symbol():
    pass

def return_list_of_dicts_of_tech_company_names_and_their_EVs_ordered_by_EV_descending():
    pass

def return_list_of_consumer_products_companies_with_EV_above_225():
    pass

def return_conglomerates_and_pharmaceutical_companies():
    pass

def return_name_and_industry_of_top_three_EV_companies():
    pass
